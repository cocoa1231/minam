#!/usr/bin/env python

from utils.geometry import Line
from PIL import Image
import numpy as np

size = width, height = 600, 600
image = Image.new("RGB", size, 'black')

lines = []


# Create the grid
for x in range(width):
    if x == width/2:
        lines.append(Line(x, 0, x, height, image, center=True))
    elif x % 20 == 0:
        lines.append(Line(x, 0, x, height, image, color=( 0, 128, 0, 255 )))

for y in range(height):
    if y == height/2:
        lines.append(Line(0, y, width, y, image, center=True))
    elif y % 20 == 0:
        lines.append(Line(0, y, width, y, image, color=( 0, 128, 0, 255 )))

# Change each color to blue
blue = 255
matrix = np.array( [[1, 1],
                    [0, 1]] )
for line in lines:
    try:
        if line.flags['center']:
            line.erase()

            start = (line.start[0] + width/2, line.start[1] - height/2)
            line_start = matrix.dot(start)
            print(start, line_start)
            end   = (line.start[0] + width/2, line.start[1] - height/2)
            line_end   = matrix.dot(end)
            print(end, line_end)

            line.redraw(line_start[0],
                        line_start[1],
                        line_end[0]  ,
                        line_end[1]  ,
                        color=(255, 255, 255, 255))
    except KeyError:
        line.redraw(line.start[0],
                    line.start[1],
                    line.end[0]  ,
                    line.end[1]  ,
                    color=(128, 128, 128, 255))

# image.thumbnail((width/2, height/2))
# image = image.resize((int(width/10), int(height/10)), Image.ANTIALIAS)

image.show()
