#!/usr/bin/env python

from PIL import Image, ImageDraw
# TODO: Delete line object once it's erased


class Line:
    def __init__(self, start_x, start_y, end_x, end_y, image, color=(0, 255, 0, 255), *args, **kwargs):
        self.background = image.getpixel((start_x, start_y))
        self.start      = (start_x, start_y)
        self.end        = (end_x, end_y)
        self.draw       = ImageDraw.Draw(image)
        self.draw.line((start_x, start_y, end_x, end_y), fill=color)

        # Giving option to pass additional flags as per use case
        self.flags = kwargs

        self.verbose = False

        # Parse **kwargs
        try:
            if kwargs['verbose'] == True or kwargs['verbose'] == 1:
                self.verbose = True
        except KeyError:
            pass

        if self.verbose:
            print("Sucessfully created line from (%d, %d) to (%d, %d)"
                    %(start_x, start_y, end_x, end_y))
    
    def erase(self, color=self.background):
        self.draw.line((self.start[0], self.start[1], self.end[0], self.end[1]),
                        self.background) # Override the line with background color
        
        if self.verbose:
            print("Erased Line")

    def redraw(self, start_x, start_y, end_x, end_y, color=128):
        self.erase()
        self.draw.line((start_x, start_y, end_x, end_y), fill=color)

        if self.verbose:
            print("Redrew line to (%d, %d) --- (%d, %d)" 
                    %( start_x, start_y, end_x, end_y ))


